job "xnginx" {
  datacenters = ["dc1"]

  group "nginx-group" {
    network {
      port "http" {
        to = 80
      }
    }

    task "nginx" {
      driver = "docker"

      config {
        image          = "nginx:1.22.1"
        ports          = ["http"]
        auth_soft_fail = true
      }

      service {
          name = "nginx"
          port = "http"
          provider = "consul"
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}