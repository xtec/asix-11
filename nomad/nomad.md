# nomad

```sh
export NOMAD_ADDR=http://192.168.56.101:4646
export CONSUL_HTTP_ADDR=http://192.168.56.101:8500
```

```sh
nomad job init redis.nomad
nomad job init -short -connect count.nomad
```

https://github.com/hashicorp/nomad/issues/12111

## Azure

To create a cluster in azure:

```pwsh
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
azure.sh

```

##

[Agent config file](https://developer.hashicorp.com/consul/docs/agent/config/config-files)

## TODO

consul autojoin: we cannt' set ip address

consul {
  server_auto_join = false
  client_auto_join = false
}


bind_addr = "0.0.0.0" # the default
advertise {
  # Defaults to the first private IP address.
  http = "1.2.3.4"
  rpc  = "1.2.3.4"
  serf = "1.2.3.4:5648" # non-default ports may be specified
}


use datacenter="dc1" on template, and remove addresses
update vm memoy to 2048 if not
box ssh -c rm .ssh/know_hosts



https://github.com/kangaroot/rootstack-devfactory
https://stackoverflow.com/questions/28686571/ansible-cant-get-inventory-hostname
https://www.cyberciti.biz/faq/turn-off-color-in-linux-terminal-bash-session/